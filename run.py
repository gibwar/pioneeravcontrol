"""
Test script for connecting to Pioneer device and attempting commands.
"""

import asyncio
import logging
import warnings

import aioconsole
from pioneeravcontrol.device import PioneerDevice
from pioneeravcontrol.consts import AvCommand

log = logging.getLogger(__name__)


def update(device):
    """ Update callback from device object. """
    log.info('power: %s', 'on' if device.power_on else 'off')
    log.info('volume: %s (%f as %%, %d raw) muted: %s', device.volume.db, device.volume.percent,
             device.volume.raw, 'yes' if device.muted else 'no')
    log.info('front panel: %s', device.front_panel.strip())
    log.info('inputs: %s', device.available_inputs)
    log.info('current input: %s', device.current_input)


async def main():
    """ Main event loop entry point """
    loop = asyncio.get_event_loop()
    wait_enter = loop.create_task(aioconsole.ainput('  *** Press enter to disconnect\n', loop=loop))

    device = PioneerDevice('172.24.44.97', loop=loop, update_callback=update)
    await device.connect()
    await asyncio.sleep(.5)
    await device.send_command(AvCommand.QUERY_SYSTEM_FIRMWARE_VERSION)

    await wait_enter

    device.disconnect()


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    warnings.simplefilter('default')
    asyncio.run(main(), debug=True)
