"""
Setuptools script for pioneeravcontrol.
"""
import sys

from setuptools import setup, find_packages

reqs = []

if 'develop' in sys.argv[1]:
    reqs += ['aioconsole', 'flake8', 'pylint']

setup(
    author='gibwar',
    author_email='gibwar@gibixonline.com',
    description='woo',
    include_package_data=True,
    install_requires=reqs,
    name='pioneeravcontrol',
    packages=find_packages(),
    url='https://gibixonline.com',
    version='1.0-alpha',
    zip_safe=True
)
