"""
Exposes various shared utility functions.
"""

import asyncio
from inspect import iscoroutinefunction

from .consts import FRONT_PANEL_LOOKUP


def parse_front_panel(data: str) -> str:
    """
    Converts the provided front panel hex string in to applicable characters.
    Returns the decoded string with the best approximations provided by the
    FRONT_PANEL_LOOKUP dictionary.
    """
    # data is a hex string, convert it in to two character chunks for
    # processing.
    split = [data[i:i + 2] for i in range(0, len(data), 2)]

    # we ignore the first chunk as it contains state information we're not
    # using right now.
    converted = [FRONT_PANEL_LOOKUP[i] for i in split[1:]]
    text = ''.join(converted)

    return text


def schedule_callback(callback, *args, loop=None) -> None:
    """
    Schedules a callback to be called on the event loop in the near future.
    `callback` can be a regular function or a coroutine. If the loop is not
    provided, the current event loop from asyncio is used.
    """
    if callback is None:
        return

    loop = loop or asyncio.get_event_loop()

    if iscoroutinefunction(callback):
        loop.create_task(callback(*args))
    else:
        loop.call_soon(callback, *args)
