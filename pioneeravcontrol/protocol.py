"""
Provides classes and functions to handle raw communication with a
Pioneer receiver. This file is not intended to be used directly.
"""

import asyncio
import logging
import re
import time
from asyncio import Protocol
from typing import Tuple

from .consts import AV_RESPONSES, AvResponse
from .utils import schedule_callback

log = logging.getLogger(__name__)


class PioneerProtocol(Protocol):
    """ Implements the asyncio.Protocol to communicate with the receiver. """
    def __init__(self, host, loop, response_callback=None, connection_lost_callback=None):
        self.__loop = loop
        self.__host = host
        self.__incomplete_line = None
        self.__response_callback = response_callback
        self.__connection_lost_callback = connection_lost_callback
        self.__transport = None
        self.last_communication = None

    def connection_made(self, transport):
        self.__transport = transport
        self.last_communication = time.time()

    def connection_lost(self, exc):
        """ Connection to the receiver was lost. """

        if exc is not None:
            log.warning('Device %s: lost connection to receiver: %s', self.__host, exc)
        else:
            log.debug('Device %s: connection closed', self.__host)

        self.__transport = None
        schedule_callback(self.__connection_lost_callback, exc, loop=self.__loop)

    def data_received(self, data):
        decoded_lines = data.decode('ascii').lstrip().splitlines(keepends=True)
        if not decoded_lines:
            return

        self.last_communication = time.time()

        if self.__incomplete_line is not None:
            decoded_lines[0] = self.__incomplete_line + decoded_lines[0]
            log.debug(
                'Device %s: prepended incomplete line with incoming data: %s',
                self.__host,
                decoded_lines[0])
            self.__incomplete_line = None

        if '\r' not in decoded_lines[-1]:
            log.debug('Device %s: incomplete command received: %s', self.__host, decoded_lines[-1])
            self.__incomplete_line = decoded_lines[-1]
            decoded_lines.pop()

        for line in [line.strip() for line in decoded_lines]:
            name, data = decode_feedback(line)
            if name is AvResponse.NONE:
                log.warning('Device %s: unable to decode feedback: %s', self.__host, line)
                continue

            if name == AvResponse.ACK:  # skip if we got a receiver acknowledgement
                continue

            log.debug('Device %s: decoded feedback: %s %s', self.__host, name, data)
            schedule_callback(self.__response_callback, name, data, loop=self.__loop)

    async def send_command(self, command):
        """
        Sends the command to the receiver. No transformation is done other than
        converting the command to ASCII and appending a final `\r` to the
        command.
        """
        encoded_command = f'{command}\r'.encode('ascii')
        log.debug('Device %s: encoded command: %s', self.__host, encoded_command)

        self.__transport.write(b'\r')
        await asyncio.sleep(0.1)
        self.__transport.write(encoded_command)


def decode_feedback(line: str) -> Tuple[AvResponse, str]:
    """ Decode the provided feedback line and extract the usable data """
    for name, exp in AV_RESPONSES.items():
        match = re.match(exp, line, re.ASCII)
        if match:
            if match.groups():
                return (name, match.group('data'))

            return (name, '')

    return (AvResponse.NONE, '')
