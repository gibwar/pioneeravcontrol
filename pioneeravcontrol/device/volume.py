"""
Contains utilities and classes to manage device volume manipulation.
"""

import re

from ..consts import VOLUME_MAX


class Volume:
    """
    Converts a raw volume value to readable units.

    Supports addition and subtraction with operands as Volume, a raw value
    (int, between 0 and VOLUME_MAX), a percentage (float, between 0 and 1),
    and a string (representing the front panel display logic). Also supports
    equality comparisons as well between the above listed conversions and
    is hashable.
    """

    __slots__ = ['_raw_volume']

    def __init__(self, volume):
        self._raw_volume = convert_volume_value(volume) or 0

    def __eq__(self, other):
        volume = convert_volume_value(other)
        if volume is not None:
            return self._raw_volume == volume

        return NotImplemented

    def __add__(self, other):
        volume = convert_volume_value(other)
        if volume is not None:
            return Volume(self._raw_volume + volume)

        return NotImplemented

    def __hash__(self):
        return hash(self._raw_volume)

    def __sub__(self, other):
        volume = convert_volume_value(other)
        if volume is not None:
            return Volume(self._raw_volume - volume)

        return NotImplemented

    def __repr__(self):
        return f'Volume({self._raw_volume})'

    @property
    def raw(self) -> int:
        """ Returns the raw value of current volume of the receiver. """
        return self._raw_volume

    @property
    def db(self) -> str:
        """
        Returns the current volume of the receiver as a string, emulating
        the value that should be displayed on the front panel. Includes the
        same +/- logic for the value as expressed in dB.
        """
        if self._raw_volume == 0:
            return '---.-dB'

        value = (self._raw_volume * .5) - 80.5
        if value == 0:
            return '0.0dB'

        return f'{value:+.3}dB'

    @property
    def percent(self) -> float:
        """
        Returns the current volume of the receiver as a percentage, always
        between 0 and 1.
        """
        return self._raw_volume / VOLUME_MAX


def convert_volume_value(volume):
    """
    Converts a provided value in to a raw volume value. The returned volume
    will always be between 0 and VOLUME_MAX, if the conversion is supported
    otherwise None is returned.

    Interpreted values are:
        Volume: returns internal raw volume.
        int: assumes the value is a raw value, and uses it as-is.
        float: expects a percentage, between 0 and 1.
        str: matches against front panel display values.
    """
    if isinstance(volume, Volume):
        return volume.raw

    raw = 0
    if isinstance(volume, int):
        raw = volume
    elif isinstance(volume, float):
        raw = volume * VOLUME_MAX
    elif isinstance(volume, str):
        match = re.match(r'([-+]\d+\.\d)dB', volume)
        if match:
            value = float(match.group(1))
            raw = (value + 80.5) / .5
        elif volume != '---.-dB':
            return None
    else:
        return None

    return int(min(max(raw, 0), VOLUME_MAX))
