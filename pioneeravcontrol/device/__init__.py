"""
The PioneerDevice class is a friendly end-user class used to help manage
the lifetime and state of a remote Pioneer AV receiver.
"""

import asyncio
import logging

from ..consts import AvCommand, AvResponse, KNOWN_INPUT_IDS
from ..protocol import PioneerProtocol
from ..utils import parse_front_panel, schedule_callback
from .volume import Volume

log = logging.getLogger(__name__)


class PioneerDevice:
    """
    Provides methods for connecting to a remote Pioneer AV device, exposing
    the current state, and managing various features of the receiver.
    """

    # This is going to be a state management class, so we'll disable the
    # attribute check.
    # pylint: disable=too-many-instance-attributes
    def __init__(self, host, port=8102, loop=None, update_callback=None):
        self._host = host
        self._port = port
        self._loop = loop or asyncio.get_event_loop()
        self._update_callback = update_callback

        # set referenced attributes so they exist
        self.connected = False
        self._transport = None
        self._protocol = None
        self._raw_volume = 0
        self._suppress_update = False
        self._current_input = None
        self._inputs = {}

        # set public state attributes to their default values
        self.power_on = False
        self.front_panel = ""
        self.muted = None

    @property
    def available_inputs(self):
        """
        Returns a list of tuples representing the available inputs and names.
        """
        return self._inputs.items()

    @property
    def current_input(self):
        """
        Returns a tuple with the id and name of the currently selected input.
        """
        return (self._current_input, self._inputs.get(self._current_input))

    @property
    def volume(self) -> Volume:
        """ Returns a Volume object representing the current volume. """
        return Volume(self._raw_volume)

    async def connect(self):
        """
        Connects to the receiver and queries the device for the current state.
        """
        log.info('Device %s: connecting to device', self._host)

        (transport, protocol) = await self._loop.create_connection(
            self._create_protocol,
            host=self._host,
            port=self._port)

        self._transport = transport
        self._protocol = protocol
        self.connected = True

        asyncio.create_task(self._query_state())

    def disconnect(self):
        """ Disconnects from the receiver. """
        if self._transport is not None:
            log.info('Device %s: closing connection', self._host)
            self._transport.close()

        self.connected = False

    async def send_command(self, command: AvCommand = None, **data):
        """ Sends a command to the receiver with the data, if provided. """
        if command is None:
            return

        if command not in AvCommand:
            raise TypeError('command must be from the AvCommand enum')

        if not self.connected or self._protocol is None:
            log.warning(
                'Device %s: command "%s" requested when not connected',
                self._host,
                command.name)
            return

        await self._protocol.send_command(command.format(**data))

    def _create_protocol(self):
        protocol = PioneerProtocol(
            self._host, self._loop, self._device_response, self._device_connection_lost)

        return protocol

    async def _device_response(self, response: str, data: str):
        log.debug('device response: %s: %s', response, data)
        notify = True
        if response == AvResponse.POWER:
            self.power_on = data == '0'
        elif response == AvResponse.FRONT_PANEL:
            self.front_panel = parse_front_panel(data)
        elif response == AvResponse.MUTE:
            self.muted = data == '0'
        elif response == AvResponse.VOLUME:
            self._raw_volume = int(data)
        elif response == AvResponse.INPUT:
            self._current_input = int(data)
        elif response == AvResponse.INPUT_NAME:
            self._handle_input_name(data)
        else:
            notify = False

        if notify and not self._suppress_update:
            schedule_callback(self._update_callback, self, loop=self._loop)

    async def _device_connection_lost(self, exc=None):
        log.info('Device %s: device connection lost (%s)', self._host, exc)
        self.connected = False
        self._transport = None
        self._protocol = None

    def _handle_input_name(self, data: str) -> None:
        """
        Processes input name response in format of XXYZ+ where XX is the input
        identification number, Y is a 0 or 1 indicating whether the input name
        is default (0, ignored) and Z+ is the text value of the input.
        """
        input_id = int(data[0:2])
        name = data[3:]

        self._inputs[input_id] = name

    async def _query_state(self):
        self._suppress_update = True
        await self.send_command(AvCommand.QUERY_POWER)
        await self.send_command(AvCommand.QUERY_VOLUME)
        await self.send_command(AvCommand.QUERY_MUTE)
        await self.send_command(AvCommand.QUERY_FRONT_PANEL)
        await self.send_command(AvCommand.QUERY_INPUT)

        for input_id in KNOWN_INPUT_IDS:
            await self.send_command(AvCommand.QUERY_INPUT_NAME, input_id=input_id)

        self._suppress_update = False
        schedule_callback(self._update_callback, self, loop=self._loop)
